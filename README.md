# VE20-COMM Venice Shops Application
This React Application powers the Venice Shops application which is live at: https://stores.veniceprojectcenter.org/

This application was developed as apart of a Worcester Polytechnic Institute Interactive Qualifying Project(IQP) team in December of 2020 as apart of SerenDPT.

If you are interested in learning more about the project please check out our [team's website](https://sites.google.com/view/ve20-comm/home)

We encourage you to read through our [Developers Guide](https://sites.google.com/view/ve20-comm/home) which also serves as a technical addendum to our team's final report. 

We'd liked to finally thank all past and future contributors to this repository!

Thank you!

The VE20-COMM team

-Lucas Fernandes, Kavim Bhatnagar, Taylor Ostrum, and Nathan Morin




